from __future__ import print_function

__author__ = "Steve Perkson"

import hidraw
import time
import struct
from numpy import ndarray

class DelayDataClass:
    #make syntax completion happy
    timeArr = ndarray((1,),int)
    dCh1 = ndarray((1,),int)
    dCh2 = ndarray((1,),int)
    dCh3 = ndarray((1,),int)
    aCh1 = ndarray((1,),int)
    aCh2 = ndarray((1,),int)
    led = ndarray((1,),int)

    def __init__(self, size):
        self.timeArr = ndarray((size,),int)
        self.dCh1 = ndarray((size,),int)
        self.dCh2 = ndarray((size,),int)
        self.dCh3 = ndarray((size,),int)
        self.aCh1 = ndarray((size,),int)
        self.aCh2 = ndarray((size,),int)
        self.led = ndarray((size,),int)

class DeviceNotConnectedException(Exception):
    pass

class InsufficientPermissionsException(Exception):
    pass

class DelayMeterConnection :
    __h = hidraw.device();
    __pcMode = False

    def __init__(self):
        devices = hidraw.enumerate();
        connected = False
        for device in devices:
            if (device['vendor_id'] == 5840 and device['product_id'] == 2110):
                connected = True

        if (not connected):
            raise DeviceNotConnectedException()

        try:
            self.__h.open(vendor_id=5840, product_id=2110)
        except IOError as e:
            raise InsufficientPermissionsException()

    def close(self):
        self.__h.close();

    def toPcMode(self):
        #82 restart
        #49 pc mode
        #48 offline mode
        self.__h.write([1, 49])
        self.__pcMode = True
        time.sleep(1)

    def toNormalMode(self):
        self.__h.write([1, 48])
        time.sleep(5)
        self.__pcMode = False

    def readArray(self, size):
        for i in range(0, 150):
            self.__h.read(14)

        ret = DelayDataClass(size)
        for i in range(0, size):
            buf = self.__h.read(14)
            ret.timeArr[i] = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
            ret.dCh1[i] = buf[4] + buf[5] << 8;
            ret.dCh2[i] = buf[6] + buf[7] << 8;
            ret.dCh3[i] = buf[8] + buf[9] << 8;
            ret.aCh1[i] = buf[10] * 240
            ret.aCh2[i] = buf[11] * 240
            ret.led[i] = buf[13]
        return ret






