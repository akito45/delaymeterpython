from __future__ import print_function
from __future__ import with_statement
from flask import Flask, make_response, request, jsonify
from threading import Lock
import numpy as np
import sys
import json
import delayMeterHid
from delayMeterHid import DeviceNotConnectedException, InsufficientPermissionsException
import signal as sg
from scipy import signal, fftpack

app = Flask(__name__)

measureLock = Lock()
global experiment_data
global calc_data
experiment_data = None
calc_data = None

def get_max_correlation(original, match, timeArr):
    z = signal.fftconvolve(original, match[::-1])
    lags = np.arange(z.size) - (match.size - 1)
    res = lags[np.argmax(np.abs(z))]
    lat = abs(res) * (timeArr[-1] - timeArr[0]) / len(timeArr)
    return lat

@app.route('/', methods = ['GET'])
def help():
    """Print available functions."""
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static':
            func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
    return jsonify(func_list)


def add_plot(ax, dataset_y, dataset_x):
    x=dataset_x.tolist()
    x0 = x[0] + 1
    x[:] = [i - x0 for i in x]
    y=dataset_y.tolist()
    if max(y) == 1:
        y[:] = [i*70000 for i in y]
    ax.plot(x, y, '-')
    return ax

@app.route('/signals_data')
def signals_data():
    global experiment_data
    if (experiment_data == None):
        return jsonify(error_code=49, error_msg="ERROR: You have not made a measurement yet!")
    data = experiment_data
    json_output = json.dumps(
        {'timestamps': data.timeArr.tolist(), 'ref': data.led.tolist(), 'video1': data.dCh1.tolist(),
         'video2': data.dCh2.tolist(), 'video3': data.dCh3.tolist(), 'audio1': data.aCh1.tolist(),
         'audio2': data.aCh1.tolist()}, sort_keys=True)
    return json_output

@app.route("/signals_img")
@app.route("/signals_img/<channel>")
def signals_img(channel=None):
    import StringIO

    global experiment_data

    if (experiment_data == None):
        return jsonify(error_code=49, error_msg="ERROR: You have not made a measurement yet!")

    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure

    fig=Figure()
    fig.set_size_inches(38.5, 10.5, forward=True)
    ax=fig.add_subplot(111)
    ax = add_plot(ax, experiment_data.led, experiment_data.timeArr)
    if (channel == None):
        ax = add_plot(ax, experiment_data.dCh1, experiment_data.timeArr)
        ax = add_plot(ax, experiment_data.dCh2, experiment_data.timeArr)
        ax = add_plot(ax, experiment_data.dCh3, experiment_data.timeArr)
        ax = add_plot(ax, experiment_data.aCh1, experiment_data.timeArr)
        ax = add_plot(ax, experiment_data.aCh2, experiment_data.timeArr)
    else:
        channel = int(channel)
        if (channel == 1):
            ax = add_plot(ax, experiment_data.dCh1, experiment_data.timeArr)
        if (channel == 2):
            ax = add_plot(ax, experiment_data.dCh2, experiment_data.timeArr)
        if (channel == 3):
            ax = add_plot(ax, experiment_data.dCh3, experiment_data.timeArr)
        if (channel == 4):
            ax = add_plot(ax, experiment_data.aCh1, experiment_data.timeArr)
        if (channel == 5):
            ax = add_plot(ax, experiment_data.aCh1, experiment_data.timeArr)
    ax.set_ylim(ymin=0)
    ax.set_xlabel('Time [ms]')
    ax.set_ylabel('Signal [arbitrary units]')
    canvas=FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    response=make_response(png_output.getvalue())
    response.headers['Content-Type'] = 'image/png'
    return response


@app.route('/latencies_data')
def latencies_data():
    global calc_data
    if (calc_data == None):
        return jsonify(error_code=49, error_msg="ERROR: You have not made a measurement yet!")
    data = calc_data
    json_output = json.dumps(
        {'timestamps': data.timeArr.tolist(), 'video1': data.dCh1.tolist(),
         'video2': data.dCh2.tolist(), 'video3': data.dCh3.tolist(), 'audio1': data.aCh1.tolist(),
         'audio2': data.aCh1.tolist()})
    return json_output

@app.route("/latencies_img")
@app.route("/latencies_img/<channel>")
def latencies_img(channel=None):
    import StringIO

    global calc_data

    if (calc_data == None):
        return jsonify(error_code=49, error_msg="ERROR: You have not made a measurement yet!")

    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure

    fig=Figure()
    fig.set_size_inches(28.5, 10.5, forward=True)
    ax=fig.add_subplot(111)
    if (channel == None):
        ax = add_plot(ax, calc_data.dCh1, calc_data.timeArr)
        ax = add_plot(ax, calc_data.dCh2, calc_data.timeArr)
        ax = add_plot(ax, calc_data.dCh3, calc_data.timeArr)
        ax = add_plot(ax, calc_data.aCh1, calc_data.timeArr)
        ax = add_plot(ax, calc_data.aCh2, calc_data.timeArr)
    else:
        channel = int(channel)
        if (channel == 1):
            ax = add_plot(ax, calc_data.dCh1, calc_data.timeArr)
        if (channel == 2):
            ax = add_plot(ax, calc_data.dCh2, calc_data.timeArr)
        if (channel == 3):
            ax = add_plot(ax, calc_data.dCh3, calc_data.timeArr)
        if (channel == 4):
            ax = add_plot(ax, calc_data.aCh1, calc_data.timeArr)
        if (channel == 5):
            ax = add_plot(ax, calc_data.aCh1, calc_data.timeArr)
    ax.set_ylim(ymin=0)
    ax.set_xlabel('Time [ms]')
    ax.set_ylabel('Latency [ms]')
    canvas=FigureCanvas(fig)
    png_output = StringIO.StringIO()
    canvas.print_png(png_output)
    response=make_response(png_output.getvalue())
    response.headers['Content-Type'] = 'image/png'
    return response

def getSampleCountFromSeconds(seconds):
    return seconds * 903; #current sample rate is fixed at 903hz for legacy reasons


@app.route('/measure')
def measure():
    try:
        with measureLock:
            global experiment_data

            try:
                conn = delayMeterHid.DelayMeterConnection();
            except DeviceNotConnectedException:
                return jsonify(error_code=52, error_msg="ERROR Device is not connected, please check connection to the LatencyKit device")
            except InsufficientPermissionsException:
                return jsonify(error_code=53, error_msg="ERROR Insufficient permissions, please check, "
                                                        "that you have moved {udevrules/99-delaymeter.rules} file to the /etc/udev/rules.d/ folder and"
                                                        " you have restarted the udev service and unplugged and plugged the device to apply the new rules ")
            conn.toPcMode();
            length = int(request.args.get('seconds', 10))
            if (length < 10):
                return jsonify(error_code = 50, error_msg="ERROR can not sample less than 10 seconds")
            if (length > 120):
                return jsonify(error_code = 51, error_msg="ERROR can not sample more than 120 seconds")
            data = conn.readArray(getSampleCountFromSeconds(length));
            conn.close();
            for i in range(0, len(data.timeArr)):
                pass
                #print("t= " + str(data.timeArr[i]) + " dCh1= " + str(data.dCh1[i]) + " aCh1= " + str(data.aCh1[i]) + " led= " + str(data.led[i]))

            timeStart = data.timeArr[0];
            for i in range(1, len(data.timeArr)):
                jump = data.timeArr[i] - data.timeArr[i - 1]
                #if (jump > 1):
                    #print(" timejump of " + str(jump) + " detected on start + " + str((data.timeArr[i-1] - timeStart) / 1000) + "s")

            #print ("lenght of timeArr " + str(len(data.timeArr)))
            experiment_data = data

            return calculate()
    except Exception as e:
        print ("got exception")
        raise e

@app.route('/latencies_avg')
def calculate():
    global experiment_data
    global calc_data
    win_len = int(request.args.get('window', 1750))

    calc_data = delayMeterHid.DelayDataClass(1000)
    res_v1 = []
    res_v2 = []
    res_v3 = []
    res_a1 = []
    res_a2 = []
    timeAxis = []
    for i in range(0, len(experiment_data.led)-win_len, 50):
        lat_v1 = get_max_correlation(experiment_data.led[i:i+win_len], experiment_data.dCh1[i:i+win_len],
                                     experiment_data.timeArr[i:i+win_len])
        lat_v2 = get_max_correlation(experiment_data.led[i:i + win_len], experiment_data.dCh2[i:i + win_len],
                                     experiment_data.timeArr[i:i + win_len])
        lat_v3 = get_max_correlation(experiment_data.led[i:i + win_len], experiment_data.dCh3[i:i + win_len],
                                     experiment_data.timeArr[i:i + win_len])
        lat_a1 = get_max_correlation(experiment_data.led[i:i + win_len], experiment_data.aCh1[i:i + win_len],
                                     experiment_data.timeArr[i:i + win_len])
        lat_a2 = get_max_correlation(experiment_data.led[i:i + win_len], experiment_data.aCh2[i:i + win_len],
                                     experiment_data.timeArr[i:i + win_len])
        timeAxis.append(i)

        res_v1.append(lat_v1)
        res_v2.append(lat_v2)
        res_v3.append(lat_v3)
        res_a1.append(lat_a1)
        res_a2.append(lat_a2)
        #print (str(i) + " - latency is: ")
        #print (str(lat_v1) + " " + str(lat_v2) + " " + str(lat_v3) + " " + str(lat_a1) + " " + str(lat_a2))

    calc_data.dCh1 = np.array(res_v1)
    calc_data.dCh2 = np.array(res_v2)
    calc_data.dCh3 = np.array(res_v3)
    calc_data.aCh1 = np.array(res_a1)
    calc_data.aCh2 = np.array(res_a2)
    calc_data.timeArr = np.array(timeAxis)
    print("Measurement result is: ")
    print("dCh1: " + str(np.mean(res_v1)) + " | Std: " + str(np.std(res_v1, ddof=1)))
    print("dCh2: " + str(np.mean(res_v2)) + " | Std: " + str(np.std(res_v2, ddof=1)))
    print("dCh3: " + str(np.mean(res_v3)) + " | Std: " + str(np.std(res_v3, ddof=1)))
    print("aCh1: " + str(np.mean(res_a1)) + " | Std: " + str(np.std(res_a1, ddof=1)))
    print("aCh2: " + str(np.mean(res_a2)) + " | Std: " + str(np.std(res_a2, ddof=1)))
    print("Measurement: DONE")

    json_output = json.dumps({'video1_latency': np.mean(res_v1), 'video1_std': np.std(res_v1, ddof=1),
         'video2_latency': np.mean(res_v2), 'video2_std': np.std(res_v2, ddof=1), 'video3_latency': np.mean(res_v3), 'video3_std': np.std(res_v3, ddof=1), 'audio1_latency': np.mean(res_a1), 'audio1_std': np.std(res_a1, ddof=1),
         'audio2_latency': np.mean(res_a2), 'audio2_std': np.std(res_a2, ddof=1)}, sort_keys=True)

    return json_output



def signal_handler(signal, frame):
        print('You pressed Ctrl+C exiting')
        sys.exit(0)

if __name__ == "__main__":
    sg.signal(sg.SIGINT, signal_handler)
    app.run(host='127.0.0.1', port=10203, threaded=True)
    #install signal handler

