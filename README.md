# Linux service for ScienceMosaic LatencyKit #
### ScienceMosaic.com - 09.2017 - v0.92 - by Steve Perkson, Tanel Ainla ###


### Installing ###
First make sure you have the latest package of �LatencyKit Linux service� available. Download from here.

1. Make sure you that you have Python 2.7 installed on your Linux machine. 
2. Make sure your Linux operating system is NOT running inside a virtual machine (VirtualBox etc.). Our service needs direct access to the USB hardware.
3. Then you need to install following debian packages:
```apt-get install python-hid```
4. Please copy following rules file to /etc/udev/rules.d/ to give our service permission to access LatencyKit USB hardware properly:
```sudo cp udevrules/99-delaymeter.rules /etc/udev/rules.d/```
5. Restart the udev service:
```sudo /etc/init.d/udev restart```
6. Unplug and replug the LatencyKit hardware.
7. Now you can run our service using:
```python servlet.py```

Once the service is started you should see following message:

``` * Running on http://127.0.0.1:10203/ (Press CTRL+C to quit)```

### Channel numbering ###

Following number scheme is used in the API to map the LatencyKit input channels:

1. V1 (Video 1) - Visual input nr. 1
2. V1 (Video 2) - Visual input nr. 2
3. V1 (Video 3) - Visual input nr. 3
4. A1 (Audio 1) - Audio input nr. 1
5. A2 (Audio 2) - Audio input nr. 3

### Features ###

Once the service is running then you can see the possible options from URL: **http://localhost:10203/**

NB! Please note that when debugging these URLs in webrowser then many browsers (Chrome for example) might run multiple request against API while you are typing the URL and it autocompletes it. This might cause Broken Pipe exceptions. When you access API with wget or some programmatic way then it�s not an issue.

All time and latency values in results are measured in milliseconds [ms].

* ```/```  - see all available API endpoints
* ```/measure?seconds=15``` - (Starts latency measurement that runs for 15 seconds). It returns JSON data structure.
* ```/signals_data``` - Returns raw sensor data from last measurement as JSON 
* ```/signals_img/5``` - Returns raw sensor data from last measurement as PNG chart for channel 5 (Audio 2)
* ```/latencies_data``` - Returns latencies data for last measurement as JSON 
* ```/latencies_avg``` - Returns latencies summary (average over the whole measurement period) as JSON
* ```/latencies_img``` - Returns latencies data from last measurement session as PNG chart for all channels
* ```/latencies_img/2``` - Returns latencies data from last measurement session as PNG chart for channel 2 (Video 2)

### Error messages ###

There are number of cases how measurement or API call can fail:

#### LatencyKit hardware is not connected to the computer ####

**Service returns error_code**: 52

**Solution is:** Please check that LatencyKit hardware is connected to your PC. Re-plug it and wait 10 seconds to make sure it has booted up before re-running the measurement command.

#### Service is started with insufficient permissions (to access LatencyKit hardware)  ####

**Service returns error_code**: 53

**Solution is:** Got through point in installation chapter again starting from step 4.

#### You try to retrieve data before you have made any measurements ####

**Service returns error_code**: 49

**Solution is:** Just run /measure and get some data to work with.

#### You try  to run measurement longer than 120 or shorter than 10 seconds ####

**Service returns error_code**: 51

**Solution is:** Just run /measure?seconds=30 with suitable measurement length.

